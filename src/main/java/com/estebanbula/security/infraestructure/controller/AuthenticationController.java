package com.estebanbula.security.infraestructure.controller;

import com.estebanbula.security.domain.usecase.AuthenticationUseCase;
import com.estebanbula.security.infraestructure.controller.dto.AuthenticationRequest;
import com.estebanbula.security.infraestructure.controller.dto.AuthenticationResponse;
import com.estebanbula.security.infraestructure.controller.dto.RegisterRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationUseCase authUseCase;

    @PostMapping("/register")
    public ResponseEntity<AuthenticationResponse> register(@RequestBody RegisterRequest request) {
        return ResponseEntity.ok(authUseCase.register(request));
    }

    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> authenticate(@RequestBody AuthenticationRequest request) {
        return ResponseEntity.ok(authUseCase.authenticate(request));
    }
}
