package com.estebanbula.security.domain.model.role;

public enum Role {
    USER,
    ADMIN
}
